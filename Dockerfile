# syntax=docker/dockerfile:1
FROM node:14.18.0
WORKDIR /vamp
ENV PORT 8081
COPY package.json /vamp/package.json
RUN npm install
COPY . /vamp/
CMD [ "node", "app.js" ]
